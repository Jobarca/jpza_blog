<?php
namespace DAWIN\JpzaBlog\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 PUISSEGUR
 *           ASSANI
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Post
 */
class Post extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * title
     *
     * @var string
     */
    protected $title = '';
    
    /**
     * summary
     *
     * @var string
     */
    protected $summary = '';
    
    /**
     * content
     *
     * @var string
     */
    protected $content = '';
    
    /**
     * publicationDate
     *
     * @var \DateTime
     */
    protected $publicationDate = null;
    
    /**
     * thumbnail
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $thumbnail = null;
    
    /**
     * tag
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DAWIN\JpzaBlog\Domain\Model\Tag>
     */
    protected $tag = null;
    
    /**
     * category
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DAWIN\JpzaBlog\Domain\Model\Category>
     */
    protected $category = null;
    
    /**
     * author
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DAWIN\JpzaBlog\Domain\Model\Author>
     */
    protected $author = null;
    
    /**
     * comment
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DAWIN\JpzaBlog\Domain\Model\Comment>
     * @cascade remove
     */
    protected $comment = null;
    
    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }
    
    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->tag = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->category = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->author = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->comment = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }
    
    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
    
    /**
     * Returns the summary
     *
     * @return string $summary
     */
    public function getSummary()
    {
        return $this->summary;
    }
    
    /**
     * Sets the summary
     *
     * @param string $summary
     * @return void
     */
    public function setSummary($summary)
    {
        $this->summary = $summary;
    }
    
    /**
     * Returns the content
     *
     * @return string $content
     */
    public function getContent()
    {
        return $this->content;
    }
    
    /**
     * Sets the content
     *
     * @param string $content
     * @return void
     */
    public function setContent($content)
    {
        $this->content = $content;
    }
    
    /**
     * Returns the publicationDate
     *
     * @return \DateTime $publicationDate
     */
    public function getPublicationDate()
    {
        return $this->publicationDate;
    }
    
    /**
     * Sets the publicationDate
     *
     * @param \DateTime $publicationDate
     * @return void
     */
    public function setPublicationDate(\DateTime $publicationDate)
    {
        $this->publicationDate = $publicationDate;
    }
    
    /**
     * Returns the thumbnail
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $thumbnail
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }
    
    /**
     * Sets the thumbnail
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $thumbnail
     * @return void
     */
    public function setThumbnail(\TYPO3\CMS\Extbase\Domain\Model\FileReference $thumbnail)
    {
        $this->thumbnail = $thumbnail;
    }
    
    /**
     * Adds a Tag
     *
     * @param \DAWIN\JpzaBlog\Domain\Model\Tag $tag
     * @return void
     */
    public function addTag(\DAWIN\JpzaBlog\Domain\Model\Tag $tag)
    {
        $this->tag->attach($tag);
    }
    
    /**
     * Removes a Tag
     *
     * @param \DAWIN\JpzaBlog\Domain\Model\Tag $tagToRemove The Tag to be removed
     * @return void
     */
    public function removeTag(\DAWIN\JpzaBlog\Domain\Model\Tag $tagToRemove)
    {
        $this->tag->detach($tagToRemove);
    }
    
    /**
     * Returns the tag
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DAWIN\JpzaBlog\Domain\Model\Tag> $tag
     */
    public function getTag()
    {
        return $this->tag;
    }
    
    /**
     * Sets the tag
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DAWIN\JpzaBlog\Domain\Model\Tag> $tag
     * @return void
     */
    public function setTag(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $tag)
    {
        $this->tag = $tag;
    }
    
    /**
     * Adds a Category
     *
     * @param \DAWIN\JpzaBlog\Domain\Model\Category $category
     * @return void
     */
    public function addCategory(\DAWIN\JpzaBlog\Domain\Model\Category $category)
    {
        $this->category->attach($category);
    }
    
    /**
     * Removes a Category
     *
     * @param \DAWIN\JpzaBlog\Domain\Model\Category $categoryToRemove The Category to be removed
     * @return void
     */
    public function removeCategory(\DAWIN\JpzaBlog\Domain\Model\Category $categoryToRemove)
    {
        $this->category->detach($categoryToRemove);
    }
    
    /**
     * Returns the category
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DAWIN\JpzaBlog\Domain\Model\Category> $category
     */
    public function getCategory()
    {
        return $this->category;
    }
    
    /**
     * Sets the category
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DAWIN\JpzaBlog\Domain\Model\Category> $category
     * @return void
     */
    public function setCategory(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $category)
    {
        $this->category = $category;
    }
    
    /**
     * Adds a Author
     *
     * @param \DAWIN\JpzaBlog\Domain\Model\Author $author
     * @return void
     */
    public function addAuthor(\DAWIN\JpzaBlog\Domain\Model\Author $author)
    {
        $this->author->attach($author);
    }
    
    /**
     * Removes a Author
     *
     * @param \DAWIN\JpzaBlog\Domain\Model\Author $authorToRemove The Author to be removed
     * @return void
     */
    public function removeAuthor(\DAWIN\JpzaBlog\Domain\Model\Author $authorToRemove)
    {
        $this->author->detach($authorToRemove);
    }
    
    /**
     * Returns the author
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DAWIN\JpzaBlog\Domain\Model\Author> $author
     */
    public function getAuthor()
    {
        return $this->author;
    }
    
    /**
     * Sets the author
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DAWIN\JpzaBlog\Domain\Model\Author> $author
     * @return void
     */
    public function setAuthor(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $author)
    {
        $this->author = $author;
    }
    
    /**
     * Adds a Comment
     *
     * @param \DAWIN\JpzaBlog\Domain\Model\Comment $comment
     * @return void
     */
    public function addComment(\DAWIN\JpzaBlog\Domain\Model\Comment $comment)
    {
        $this->comment->attach($comment);
    }
    
    /**
     * Removes a Comment
     *
     * @param \DAWIN\JpzaBlog\Domain\Model\Comment $commentToRemove The Comment to be removed
     * @return void
     */
    public function removeComment(\DAWIN\JpzaBlog\Domain\Model\Comment $commentToRemove)
    {
        $this->comment->detach($commentToRemove);
    }
    
    /**
     * Returns the comment
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DAWIN\JpzaBlog\Domain\Model\Comment> $comment
     */
    public function getComment()
    {
        return $this->comment;
    }
    
    /**
     * Sets the comment
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DAWIN\JpzaBlog\Domain\Model\Comment> $comment
     * @return void
     */
    public function setComment(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $comment)
    {
        $this->comment = $comment;
    }

}