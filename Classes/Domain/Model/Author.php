<?php
namespace DAWIN\JpzaBlog\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 PUISSEGUR
 *           ASSANI
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Author
 */
class Author extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * firstName
     *
     * @var string
     */
    protected $firstName = '';
    
    /**
     * lastName
     *
     * @var string
     */
    protected $lastName = '';
    
    /**
     * userName
     *
     * @var string
     */
    protected $userName = '';
    
    /**
     * presentation
     *
     * @var string
     */
    protected $presentation = '';
    
    /**
     * tag
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DAWIN\JpzaBlog\Domain\Model\Tag>
     */
    protected $tag = null;
    
    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }
    
    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->tag = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }
    
    /**
     * Returns the firstName
     *
     * @return string $firstName
     */
    public function getFirstName()
    {
        return $this->firstName;
    }
    
    /**
     * Sets the firstName
     *
     * @param string $firstName
     * @return void
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }
    
    /**
     * Returns the lastName
     *
     * @return string $lastName
     */
    public function getLastName()
    {
        return $this->lastName;
    }
    
    /**
     * Sets the lastName
     *
     * @param string $lastName
     * @return void
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }
    
    /**
     * Returns the userName
     *
     * @return string $userName
     */
    public function getUserName()
    {
        return $this->userName;
    }
    
    /**
     * Sets the userName
     *
     * @param string $userName
     * @return void
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;
    }
    
    /**
     * Returns the presentation
     *
     * @return string $presentation
     */
    public function getPresentation()
    {
        return $this->presentation;
    }
    
    /**
     * Sets the presentation
     *
     * @param string $presentation
     * @return void
     */
    public function setPresentation($presentation)
    {
        $this->presentation = $presentation;
    }
    
    /**
     * Adds a Tag
     *
     * @param \DAWIN\JpzaBlog\Domain\Model\Tag $tag
     * @return void
     */
    public function addTag(\DAWIN\JpzaBlog\Domain\Model\Tag $tag)
    {
        $this->tag->attach($tag);
    }
    
    /**
     * Removes a Tag
     *
     * @param \DAWIN\JpzaBlog\Domain\Model\Tag $tagToRemove The Tag to be removed
     * @return void
     */
    public function removeTag(\DAWIN\JpzaBlog\Domain\Model\Tag $tagToRemove)
    {
        $this->tag->detach($tagToRemove);
    }
    
    /**
     * Returns the tag
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DAWIN\JpzaBlog\Domain\Model\Tag> $tag
     */
    public function getTag()
    {
        return $this->tag;
    }
    
    /**
     * Sets the tag
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\DAWIN\JpzaBlog\Domain\Model\Tag> $tag
     * @return void
     */
    public function setTag(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $tag)
    {
        $this->tag = $tag;
    }

}