<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'DAWIN.' . $_EXTKEY,
	'Pi1',
	array(
		'Post' => 'list, show',
		'Author' => 'list, show',
		'Tag' => 'list, show',
		'Category' => 'list, show',
		'Comment' => 'create',
		
	),
	// non-cacheable actions
	array(
		'Comment' => 'create',
		
	)
);
